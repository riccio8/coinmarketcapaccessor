﻿namespace CoinmarketcapAccessor
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Npgsql;

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Hello World!");
                //new Program().RunAsync();
                new Program().FillBalance();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
        }

        
        public Dictionary<string, decimal> GetCoins()
        {
            var d = new Dictionary<string, decimal>();
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                var sql = $"select price, coin_id from coin";

                using (var cmd = new NpgsqlCommand(sql, con))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            //yield return ((string)reader[0]).Trim();
                            d[((string)reader[1]).Trim()] = ((decimal)reader[0]);
                    }
                }
            }
            return d;
        }

        public HashSet<string> GetExchanges()
        {
            HashSet<string> d = new HashSet<string>();
            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                var sql = $"select exchange_id from exchange";

                using (var cmd = new NpgsqlCommand(sql, con))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            d.Add(((string)reader[0]).Trim());
                    }
                }
            }
            return d;
        }


        private void FillBalance()
        {

            using (var con = new NpgsqlConnection(connectionString))
            {
                con.Open();
                var dbcmd = con.CreateCommand();

                var cn = GetCoins();
                var ex = GetExchanges();

                string[] clients = new string[] { "dyatel", "dyatel1", "dyatel2" };
                decimal[] money = new decimal[] { 4500m, 9500m, 20000m };
                int m = 0;
                foreach (var client_id in clients)
                {
                    dbcmd.CommandText = "delete from balance where client_id = '" + client_id + "'";
                    dbcmd.ExecuteNonQuery();

                    Random rr = new Random();
                    int i = 1;

                    foreach (var item in cn)
                    {
                        foreach (var exchange_id in ex)
                        {
                            decimal price = item.Value;
                            string coin_id = item.Key;
                            decimal pos = (money[m] + (decimal)rr.Next(1500)) / price;
                            string sql = string.Format($"insert into balance (client_id, coin_id, exchange_id, position) " +
                               "VALUES('" + client_id + "', '" + coin_id + "', '" + exchange_id + "'," + pos + ") ");
                            dbcmd.CommandText = sql;
                            dbcmd.ExecuteNonQuery();
                        }
                        i++;
                    }

                    m++;
                }

            }
        }

        private async void RunAsync()
        {
            string url = "https://api.coinmarketcap.com/v2/ticker/?limit=50"; // 96% of all daily trading volume
            using (HttpClient client = new HttpClient())
            {
                //var result = client.GetAsync("http://aspnetmonsters.com").Result;
                HttpResponseMessage responce = client.GetAsync(url).Result;
                if (responce.IsSuccessStatusCode)
                {
                    string result = await responce.Content.ReadAsStringAsync();
                    var root = JsonConvert.DeserializeObject<JToken>(result);

                    using (var con = new NpgsqlConnection(connectionString))
                    {
                        con.Open();
                        var dbcmd = con.CreateCommand();

                        dbcmd.CommandText = "delete from coin";
                        dbcmd.ExecuteNonQuery();

                        JToken token = root["data"];
                        foreach (JProperty prop in token)
                        {
                            dynamic t = prop.Value;
                            string name = t.name;
                            string symbol = t.symbol;
                            int rank = t.rank;
                            decimal cap = t.quotes.USD.market_cap;
                            decimal vol = t.quotes.USD.volume_24h;
                            decimal price = t.quotes.USD.price;

                            string sql = string.Format($"insert into coin (coin_id, rank, name, fiat, market_cap,volune_24,price) " +
                               "VALUES('" + symbol + "', " + rank + ", '" + name + "', False, " + cap + "," + vol + "," + price + ") ");
                            dbcmd.CommandText = sql;
                            dbcmd.ExecuteNonQuery();
                        }
                    }
                }

            }
        }


        private string connectionString = "Host = cems.cox6rcin7q4s.us-east-1.rds.amazonaws.com; Port=5432; Username=developer; Password=develler667_; Database=postgres";
    }
}
