﻿namespace CoinmarketcapAccessor
{
    //    {
    //            "id": 1765, 
    //            "name": "EOS", 
    //            "symbol": "EOS", 
    //            "website_slug": "eos", 
    //            "rank": 5, 
    //            "circulating_supply": 896149492.0, 
    //            "total_supply": 900000000.0, 
    //            "max_supply": 1000000000.0, 
    //            "quotes": {
    //                "USD": {
    //                    "price": 7.59253, 
    //                    "volume_24h": 684249000.0, 
    //                    "market_cap": 6804041904.0, 
    //                    "percent_change_1h": -2.14, 
    //                    "percent_change_24h": -5.61, 
    //                    "percent_change_7d": -26.78
    //                }
    //            }, 
    //            "last_updated": 1530221235
    //        }

    public class USD
    {
        public double price { get; set; }
        public double volume_24h { get; set; }
        public double market_cap { get; set; }
        public double percent_change_1h { get; set; }
        public double percent_change_24h { get; set; }
        public double percent_change_7d { get; set; }
    }

    public class Quotes
    {
        public USD USD { get; set; }
    }

    public class CoinInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public string symbol { get; set; }
        public string website_slug { get; set; }
        public int rank { get; set; }
        public double circulating_supply { get; set; }
        public double total_supply { get; set; }
        public double max_supply { get; set; }
        public Quotes quotes { get; set; }
        public int last_updated { get; set; }
    }
}
